package bst;

import java.util.LinkedList;
import java.util.List;
/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 5
 * 
 * @author Olena Khomenko <br>
 * 
 *         Binary search tree based implementation StudentDictionary Keeps
 *         specified information about students
 * 
 *         each node contains id (number of student's card) and information
 *         about student (name, surname etc.)
 * 
 *         all search, delete and get operation use unique id as a key
 * 
 * @param <E>
 */

public class BSTree<E extends Student> implements StudentDictionary<E> {

	/**
	 * root of a tree
	 */
	private TreeNode<E> root;

	public BSTree(TreeNode<E> x) {
		this.root = x;
	}
	public BSTree() {
		
	}
	/**
	 * Returns true if this dictionary (binary search tree) contains an student
	 * for the specified cardNumber
	 * 
	 * @param cardNumber
	 *            cardNumber whose presence in this tree is to be tested
	 * @return true if this tree contains an student record for the specified
	 *         cardNumber
	 */
	@Override
	public boolean containsKey(int cardNumber) {
		// TODO
		// Error: if a cardNumber <= 0 return false
		if(cardNumber <= 0) return false;
		TreeNode<E> x = this.root;
		while(x != null) {
			if (cardNumber < x.getKey()) x = x.left;
			else if(cardNumber > x.getKey()) x = x.right;
			else return true;
		}
		return false;
	}

	/**
	 * Returns the number of nodes in this tree.
	 * 
	 * @return he number of nodes in this tree
	 */
	@Override
	public int size() {
	  BSTree<E> right = new BSTree<E>(this.root.right);
	  BSTree<E> left = new BSTree<E>(this.root.left);
	  int c = 1;                                     
	  if ( right != null ) c += right.size();        
	  if ( left != null ) c += left.size();
	  return c;
	}

	/**
	 * Returns the student to which the specified cardNumber is associated, or
	 * null if this tree contains no student for the cardNumber.
	 * 
	 * @param cardNumber
	 *            the cardNumber whose associated student is to be returned
	 * @return the student with the specified cardNumber, or null if this tree
	 *         contains no student for the cardNumber or cardNumber is invalid
	 *         (negative or 0)
	 */
	@Override
	public E get(int cardNumber) {
		if(cardNumber <= 0) return null;
		TreeNode<E> x = this.root;
		while(x != null) {
			if (cardNumber < x.getKey()) x = x.left;
			else if(cardNumber > x.getKey()) x = x.right;
			else return x.st;
		}
		return null;
	}

	public TreeNode<E> getTreeNode(int cardNumber) {
		if(cardNumber <= 0) return null;
		TreeNode<E> x = this.root;
		while(x != null) {
			if (cardNumber < x.getKey()) x = x.left;
			else if(cardNumber > x.getKey()) x = x.right;
			else return x;
		}
		return null;
	}
	/**
	 * Removes the student for this cardNumber from this tree if present.
	 * 
	 * @param cardNumber
	 *            cardNumber for which student should be removed
	 * @return the previous student associated with cardNumber, or null if there
	 *         was no student for cardNumber.
	 */
	@Override
	public E remove(int cardNumber) {
		if (this.containsKey(cardNumber) || root == null) {
			if (root.getKey() == cardNumber) {
				TreeNode<E> temp = new TreeNode<E>(null);
				temp.left = root;
	
				E result = root.remove(cardNumber, temp);
				root = temp.left;
				return result;
			} else return root.remove(cardNumber, null);
		}
		return null;
	}

	// use this method when remove nodes
//	private void replaceSubTree(TreeNode<E> u, TreeNode<E> v) {
//		if (u == root) {
//			root = v;
//		} else {
//			TreeNode<E> par = u.parent;
//			if (par.isLeftChild(u)) {
//				par.setLeft(v);
//			} else {
//				par.setRight(v);
//			}
//		}
//	}

	/**
	 * Remove from a tree all students that satisfy specified criteria
	 * 
	 * @param removeCriteria
	 *            one or more criteria by witch students will be removed from
	 *            this tree
	 * @return number of students was be removed
	 */
	public int remove(int course, boolean Military) {
		// TODO
		// 1) find all nodes witch students satisfy specified removeCriteria
		// save in a list nodes to be removed
		// 2) call method remove (node) for each node in a list
		// 3) call size method to check successful removing
		// 4) return difference between old size and new size
		List<TreeNode<E>> toRemove = new LinkedList<TreeNode<E>>();
		findToRemove(course, Military, toRemove);
		for (int i = 0; i < toRemove.size(); i++){
			//remove(toRemove.get(i).getKey());
			toRemove.get(i).remove();
		}
		return toRemove.size();
	}

	public void findToRemove(int course, boolean Military, List<TreeNode<E>> toRemove){
		TreeNode<E> x = this.root;
		if (x != null) {
			BSTree<E> right = new BSTree<E>(x.right);
			BSTree<E> left = new BSTree<E>(x.left);
			left.findToRemove(course, Military, toRemove);
			right.findToRemove(course, Military, toRemove);
			if(x.st.getCourse() == course && x.st.getMilitary() == Military){
				toRemove.add(x);
			}
		}
	}

	/**
	 * Returns true if this dictionary contains no key-value mappings
	 * 
	 * @return true if this dictionary contains no key-value mappings
	 */
	@Override
	public boolean isEmpty() {
		// TODO
		if (root == null)
		return true;
		return false;
	}

	/**
	 * Associates the specified student with the specified cardNumber in this
	 * dictionary. If the dictionary previously contained a mapping for the
	 * cardNumber, the old student is replaced by the specified student.
	 * 
	 * @param num
	 *            cardNumber with which the specified student is to be
	 *            associated
	 * @param s
	 *            student to be associated with the specified cardNumber
	 * @return the previous student associated with cardNumber, or null if there
	 *         was no mapping for key
	 */
	@SuppressWarnings("unchecked")
	@Override

	 public E put(Student s) {
         if (root == null) {
               root = new TreeNode<E>((E) s);
               return null;
         } else
               return root.add((E)s);
   }
	/**
	 * Outputs dictionary elements in table form
	 */

	@Override
	public void printDictionary() {
		// TODO
		// Invoke private recursive method for traversing a tree. Type of
		// traversing is defined in your variant
		// preorder: ������ �������
		// postorder: ��������� �������
		// inorder: ����������� �������
		TreeNode<E> x = this.root;;
		if (x != null) {
			System.out.println(x.toString());
		    BSTree<E> right = new BSTree<E>(x.right);
		    BSTree<E> left = new BSTree<E>(x.left);
		    left.printDictionary();
		    right.printDictionary();
		}
	}

	@SuppressWarnings("hiding")
	class TreeNode<E extends Student> {
		/**
		 * information about student. Instance of class Student
		 */
		private E st;

		/**
		 * reference to the right node
		 */
		private TreeNode<E> right;

		/**
		 * reference to the left node
		 */
		private TreeNode<E> left;
		/**
		 * reference to the parent node
		 */

		private TreeNode<E> parent;

		public TreeNode(E e) {
			this.st = e;
			this.parent = null;
			this.left = null;
			this.right = null;
			// TODO: complete initialization

		}

		public TreeNode(E s, TreeNode<E> parent) {
			this.st = s;
			this.parent = parent;
			this.left = null;
			this.right = null;
			// TODO: complete initialization

		}

		
		public E add(E s) {
            if (s.getKey() == this.getKey()) {
            	E old = this.st;
            	this.st = s;
                return old;
            } else if (s.getKey() < this.getKey()) {
                  if (left == null) {
                        left = new TreeNode<E>(s);
                        return null;
                  } else
                        return left.add(s);
            } else if (s.getKey() > this.getKey()) {
                  if (right == null) {
                        right = new TreeNode<E>(s);
                        return null;
                  } else
                        return right.add(s);
            }
            return null;
		}
		
		public E remove() {

//			E temp = this.st;
//			// nullpointer ex
//			if(parent == null) {
//				if (right != null) {
//					this.st = right.getMinimum().st;
//					right.remove(this.getKey(), this);
//				} else if (left != null) {
//					left.remove(this.getKey(),this);
//				} else 
//					return null;
//			} else {
//			if (left != null && right != null) {
//				this.st = right.getMinimum().st;
//				right.remove(this.getKey(), this);
//			} else if (parent.left == this) {
//				parent.left = (left != null) ? left : right;
//			} else if (parent.right == this) {
//				parent.right = (left != null) ? left : right;
//			}
//			}
			return temp;
		}
		
		public E remove(int cardNumber, TreeNode<E> parent) {
			if (cardNumber < this.getKey()) {
				if (left != null)
					return left.remove(cardNumber, this);
				else
					return null;
			} else if (cardNumber > this.getKey()) {
				if (right != null)

					return right.remove(cardNumber, this);
				else
					return null;
			} else {
				E temp = this.st;
				if (left != null && right != null) {
					this.st = right.getMinimum().st;
					right.remove(this.getKey(), this);
				} else if (parent.left == this) {
					parent.left = (left != null) ? left : right;
				} else if (parent.right == this) {
					parent.right = (left != null) ? left : right;
				}
				return temp;
			}
		}

		public int getKey() {
			return st.getKey();
		}

		public E getValue() {
			return st;
		}

		public TreeNode<E> addLeftChild(E s) {
			TreeNode<E> x = new TreeNode<E>(s);
			setLeft(x);
			return x;
		}

		public TreeNode<E> addRightChild(E s) {
			TreeNode<E> x = new TreeNode<E>(s);
			setRight(x);
			return x;
		}

		public void setLeft(TreeNode<E> left) {
			this.left = left;
		}

		public void setRight(TreeNode<E> right) {
			this.right = right;
		}

		public boolean isLeftChild(TreeNode<E> node) {
			if (node.parent.getKey() > node.getKey())
			return true;
			return false;
		}

		public boolean isRightChild(TreeNode<E> node) {
			if (node.parent.getKey() > node.getKey())
				return true;
				return false;
		}

		public TreeNode<E> getMinimum() {
			TreeNode<E> x = this;
			while (x.left != null) {
				x = x.left;
			}
			return x;

		}
		
		public TreeNode<E> getMax() {
			TreeNode<E> x = this;
			while (x.right != null) {
				x = x.right;
			}
			return x;

		}
		
		@Override
		public String toString() {
			return st.toString();
		}

	}

}
