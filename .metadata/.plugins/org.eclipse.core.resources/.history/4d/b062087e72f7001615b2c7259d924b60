package lab2s;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;



/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 1
 * 
 * @author Olena Khomenko
 * 
 *         This is a program skeleton for lab 1 (task1): <br>
 *         1) Read a csv file <br>
 *         2) Create the array of students and check data from the file<br>
 *         3) Output the array in a table view <br>
 *         4) Sort the array of students <br>
 *         5) Output sorted array in a table view
 * 
 *         Write your code in the places of the methods which are marked by TODO
 *         marker
 * 
 */

public class SorterTask2 {
	// name of file which stores data about students
	private static String fileName = "students.csv";

	// path to the current directory that contains directory "data"
	private static String currenDir = System.getProperty("user.dir")
			+ File.separatorChar + "data";

	private static DLNode List = null;
	public static void main(String[] args) {
		CSVReader reader = null;
		String path = Paths.get(currenDir, fileName).toString();

		// read data from a file "students.csv"
		try {

			reader = new CSVReader(new FileReader(path));
			System.out.println("File \"" + path + " \"  have been reading ");

			// 1) read all lines from a file
			List<String[]> list = reader.readAll();

			if (!list.isEmpty()) {
				int numLines = list.size();
				System.out.println("File contains " + numLines + "  lines");

				// 2) create the array of students and check data from the file
				List = createListOfStudents(list);

			} else {
				System.out.println("Error: file  " + path + "   is empty");
			}

			reader.close();

		} catch (FileNotFoundException e) {
			System.out.println("Error: file  " + path + "   not found");
		} catch (IOException e) {
			System.err.println("Error:read file " + path + "   error");
		} catch (SecurityException e) {
			System.err.println(e.getMessage());
		}

		// 3) Output the array in a table view
		if (List != null) {
			System.out.println("\nBefore sorting\n");
			printStudents(List);

		// 4) Sort the array of students
			sort(List);

		// 5) Output sorted array in a table view
			System.out.println("\nAfter sorting\n");
			printStudents(List);
		}
	}

	/**
	 * Checks validity data in the specified list of string array, creates array
	 * and fill the array by object of class Student using only valid data
	 * 
	 * @param list
	 *            the list of array of strings
	 * @return array of students or empty array if all items in the list are not
	 *         valid
	 */
	public static DLNode createListOfStudents(List<String[]> list) {

		for (Iterator<String[]> it = list.iterator(); it.hasNext();) {
			
			String[] line = it.next();
			
			DLNode node = createDLNode();

			node.data.surname = line[0];
			node.data.name = line[1];
			node.data.form = line[2];
			
			List = addNode(List,node);
			
		}
		return List;
	}

	private static DLNode createDLNode() {
		DLNode newNode = new DLNode();
		newNode.next = null;
		newNode.prev = null;
		return newNode;

	}
	
	private static DLNode addNode(DLNode head, DLNode node) {
		if (null == head){
			head = node;
		} else {
			DLNode tail = head;
		    while(tail.next != null)
			tail = tail.next;
		    addAfter(tail, node);
		}
		return head;
	}
	
	private static void addAfter (DLNode y, DLNode x){
	    x.next = y.next;
	    x.prev = y;
	    if(y.next != null) {
	    	y.next.prev = x;
	    }
	    y.next = x;
	}

	/**
	 * Outputs the table output array of student in table form: each element is
	 * on a separate row. Each row begins with a field by which sorting was
	 * performed Table should have a header with field's names.
	 * 
	 * @param stds
	 *            array of students to be output
	 */
	public static void printStudents(DLNode studs) {
		// TODO
		// check is the studs is null reference
		// check if the length of array is 0
		// output array of students invoking method print from class Student
			
			DLNode x = studs;
			if (x == null) {
			    System.out.printf("The list is empty!\n");
			} else {
				System.out.println("|||||||||||||||||||||||||||||||||||||||");
			    while (x != null) {
			    	x.data.print();
					x = x.next;
			    }
			    System.out.println("|||||||||||||||||||||||||||||||||||||||");
			}
		
	}

	/**
	 * Sorts the specified array of objects into order specified in the variant.
	 * To compare two elements method compare must be invoked.
	 * 
	 * @param students
	 *            the array to be sorted
	 */
	public static void sort(DLNode h) {
		// TODO

		// Make the first node the head of the sorted list.
		DLNode sortedList = h;
		h = h.next;
		sortedList.clearLinks();
		// Traverse the given linked list and insert every
		// node to sorted list
		while (h != null) {
			DLNode insertedEl = h;
			h = h.next;
			insertedEl.clearLinks();
			sortedList = insertToSortedList(sortedList, insertedEl);
		}
	}
	
	private static DLNode insertToSortedList(DLNode h, DLNode n) {
		if (h == null) {
			return n;
		}
		// Special case if node should be inserted at the beginning of the list
		if (compare(h.data,n.data) == -1) {
			return addFirst(h);
		}
		// Search list for correct position of n node.
		DLNode search = h;
		while (search.next != null && compare(n.data,search.next.data) == 1) {
			search = search.next;
		}
		// n node goes after search.
		addAfter(search, n);

		return h;

	}
	
	
	public static DLNode addFirst(DLNode h) {
		DLNode newNode = new DLNode();
		newNode.prev = null;
		newNode.next = h;
		if (h != null) {
			h.prev = newNode;
		}
		return newNode;
	}
	
	/**
	 * Compares its two arguments for order. Returns a -1, 0, or 1 as the first
	 * argument is less than, equal to, or greater than the second.
	 * 
	 * @param s1
	 *            the first object of class Student to be compared
	 * @param s2
	 *            the second object of class Student to be compared
	 * @return -1, 0, or 1 as the first argument is less than, equal to, or
	 *         greater than the second
	 */
	private static int compare(Student s1, Student s2) {
		// TODO
		int f1 = s1.form.charAt(0);
		int f2 = s2.form.charAt(0);
		
		if(f1 == f2) {
			return compareSur(s1,s2);
		} else if(f1 > f2) {
			return 1;
		} else
			return -1;
	}

	private static int compareSur(Student s1, Student s2){
		int flag = 0;
		int min = s1.surname.length();
		if (min > s2.surname.length()) min = s2.surname.length();
		for (int i = 0; i < min; i++) {
			int sur1 = s1.surname.charAt(i);
			int sur2 = s2.surname.charAt(i);
			if(sur1 == sur2) {
				continue;
			} else if(sur1>sur2) {
				flag = 1;
				break;
			} else {
				flag = -1;
				break;
			}
		}
		if (flag == 0 && s1.surname.length() != s2.surname.length()) {
			return 1;
		} else {
			return flag;
		}
	}
}

