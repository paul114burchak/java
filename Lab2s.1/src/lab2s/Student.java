package lab2s;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 1
 * 
 * @author Olena Khomenko
 * 
 *         Represents information about student: its name and number of course <br>
 *         This class is a sample how to define class, fields and methods
 * 
 *         Rewrite this class and its methods <br>
 *         Choose information to be saved in a class from lab manuals (table 1,
 *         col.2).<br>
 * 
 *         Write methods setXXX to set specified value to the field XXX. <br>
 * 
 *         Write method print to output information about student (values of the
 *         fields) in formatted string. <br>
 * 
 *         Write static methods boolean isValidXXX to check whether specified
 *         string could set (or convert and set) to the field XXX
 * 
 */

public class Student {
	String surname;
	
	String name;
	/**
	 * form (budget/contract)
	 */
	String form;
	/**
	 * Sets the name of a student
	 * 
	 * @param name
	 *            string specified the name
	 */
	public void setSurName(String surname) {
		this.surname = surname;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * Outputs formatted values of fields in standard output
	 * 
	 */
	public void print() {
		System.out.printf("|| %-10s| %-10s| %-10s||\n", surname, name, form);
	}
	/**
	 * Determines if the specified string is a student's name. This string is
	 * valid if it contains all alphabet letters and begins from uppercase
	 * letter
	 * 
	 * @param name
	 *            the string to be tested
	 * @return true if the specified string is a name, false otherwise.
	 */
	public boolean isValidName(String name) {
		if (!Character.isUpperCase(name.charAt(0))) {
			return false;
		}
		for (int i = 1; i < name.length(); i++) {
			if (!Character.isAlphabetic(name.charAt(i)))
				return false;
		}
		return true;
	}

	public boolean isValidSurName(String surname) {
		if (!Character.isUpperCase(surname.charAt(0))) {
			return false;
		}
		for (int i = 1; i < surname.length(); i++) {
			if (!Character.isAlphabetic(surname.charAt(i)))
				return false;
		}
		return true;
	}
	
	public boolean isValidForm(String form) {
		if (form.equals("budget") || form.equals("contract")) {
			return true;
		} else {
			return false;
		}
	}
}
