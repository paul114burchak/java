package lab2s;

import lab2s.Student;
/**
 * KPI- FPM - PZKS <br>
 * Course: Algorithms and Data Structures (1)
 *
 * @author Olena Khomenko
 * @version 2016-12-11
 * 
 *          This class represents node in a double-linked list.
 *
 */
public class DLNode {
	Student data; // each node keeps the Student data
	
	public DLNode() {
		data = new Student();
    }
	
	DLNode next; // reference to the next node
	DLNode prev; // reference to the previos
	/**
	 * sets to null links to the next and previous nodes
	 */
	void clearLinks() {
		this.next = null;
		this.prev = null;
	}
}
