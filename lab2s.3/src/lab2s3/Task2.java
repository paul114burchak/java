package lab2s3;

public class Task2 {
	public static void main (String[] args) {
		Stack stack = new LinkedStack();
		stack.push(6);

		stack.print();
		
		stack.pop();
		stack.push(4);
		stack.push(0);stack.push(-1);stack.push(200);stack.push(-10);
		
		if (stack.size() == 5)
		System.out.println("test passed");

		if (stack.isEmpty()) System.out.println("empty");
		else System.out.println("not empty");
		
		stack.print();
		stack.pop();
		stack.pop();
		stack.pop();
		stack.pop();
		stack.pop();
		
		if (stack.isEmpty()) System.out.println("empty");
		else System.out.println("not empty");
		System.out.println(stack.pop());
	}
}