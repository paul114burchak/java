package lab2s3;

public class Task1 {
	public static void main (String[] args) {
		List list = new ArrayList(2);
		list.add("0");
		list.add(0,"1");
		if (list.get(1) == "0") System.out.println(true);
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");
		list.add("66");
		list.add("7");
		list.add("8");
		if (list.size() == 9) System.out.println(true);
		
		list.print();
		list.remove(0);
		if (list.get(0) == "0") System.out.print(true);
		
		System.out.println("\n");
		list.print();
		list.set(list.size()-1, "200");
		
		if (list.size() == 8) System.out.println(true);
		if (list.get(7) == "200") System.out.print(true);
		
		list.set(0, "100");
		list.set(-1, "0");
		list.set(100, "99");
		if (list.remove(7) == "200") System.out.print(true);
		list.remove(list.size()-1);
		
		System.out.println("\n");
		list.print();
		list.add(0,"55");
		list.add(6,"55");
		if (list.get(list.size()-2) == "55") System.out.print(true);
		if (list.get(list.size()-1) == list.get(7)) System.out.print(true);
		list.remove("100");
		
		System.out.println("\n");
		list.print();

		String s = new String();
		s = list.get(-1);
		if (s != null) System.out.printf("%s\n",s);
		else System.out.println("Index out of bounds");
		if (list.get(0) == "55") System.out.print(true);
	}
}
