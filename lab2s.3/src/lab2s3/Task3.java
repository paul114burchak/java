package lab2s3;

import java.util.Random;

public class Task3 {
	public static void main (String[] args) {
		int n = 10;
		List list = new ArrayList(n);
		Stack stack = new LinkedStack();
		Random rand = new Random();
		
		for (int i = 0; i < n; i++) {
			int element = rand.nextInt(100);
			list.add(Integer.toString(element));
		}
		
		for (int i = 0; i < list.size(); i++) {
			int element = Integer.parseInt(list.get(i));
			if ( element > 50 && element % 2 == 1) {
				stack.push(element);
				list.remove(i--);
			}
		}
		System.out.println("List:");
		list.print();
		System.out.println("Stack:");
		stack.print();
	}
}