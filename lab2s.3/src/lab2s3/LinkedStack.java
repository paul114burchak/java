package lab2s3;

import lab2s3.SLNode;

public class LinkedStack implements Stack {
	private SLNode top;

	@Override
	public boolean isEmpty() {
		// TODO
		if (size() == 0) return true;
		return false;
	}

	@Override
	public int pop() {
		// TODO
		if (isEmpty()) System.exit(1);
		//if (isEmpty()) throw new NullPointerException();
		int item = top.data;
		top = top.next;
		return item;
	}

	@Override
	public void push(int item) {
		// TODO
		top = new SLNode(item, top);
	}

	@Override
	public int top() {
		// TODO
		if (isEmpty()) System.exit(1);
		return top.data;
	}
	
	@Override
	public int size() {
		// TODO
		int count = 0;
		SLNode cur = top;
		while (cur != null) {
			count++;
			cur = cur.next;
		}
		return count;
	}
	
	public void print() {
		SLNode x = top;
		while (x != null) {
			System.out.println(x.data);
			x = x.next;
		}
	}

}
