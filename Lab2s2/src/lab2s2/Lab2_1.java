package lab2s2;

import java.io.*;
import java.util.Random;
import java.util.Scanner;
public class Lab2_1 {
	private static final int N = 1000000;
	public static void main (String[] args) {
		int[] arr = new int[N];
		Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(N/200);
        }
        
        String num = String.valueOf(N);
        String filename = "Task2"+num+".txt";
        
        try {
        	writeArray(filename, arr);
        } catch (IOException e) {
            e.printStackTrace();
        }

		//arr = countingSort(arr,N/200);  //               sorted
		
        //for (int i = 0; i < N/2; i++) swap(arr,i,N-1-i);
		//for (int i = 0, mid = N >> 1, j = N - 1; i < mid; i++, j--) swap(arr, i, j);
		
        
		double before = System.currentTimeMillis();
		
		insertionSort(arr);

		double after = System.currentTimeMillis();
		
		
		System.out.print("Time of insertion sorting:");
		double res = after-before;
		System.out.print(res);
		
		arr = readArray(filename, N);
		

        //for (int i = 0; i < N/2; i++) swap(arr,i,N-1-i); //    reverse
		
		before = System.currentTimeMillis();
		
		arr = countingSort(arr,N/200);

		after = System.currentTimeMillis();
		
		System.out.print("\nTime of counting sorting:");
		res = after-before;
		System.out.print(res);
	}
	
	private static int[] readArray(String file, int n){
        try{
            String buff;
            Scanner s = new Scanner(new File(file));
            int[] array = new int[n];
            for (int i = 0; i < n; i++){
                buff = s.next();
                array[i] = Integer.parseInt(buff);
            }
            s.close();
            return array;
        }
        catch (FileNotFoundException e){

        }
        return null;
    }
	
	public static void insertionSort(int[] ar) {
		for (int i = 1; i < N; i++) {
			int j = i - 1;
			int key = ar[i];
			while (j >= 0 && ar[j] > key) {
				ar[j + 1] = ar[j];
				j--;
			}
			ar[j + 1] = key;
		}
	}
	
	public static void writeArray (String filename, int[] arr) throws IOException{
        BufferedWriter outputWriter = null;
        outputWriter = new BufferedWriter(new FileWriter(filename));
        for (int i = 0; i < arr.length; i++) {
            outputWriter.write(arr[i]+"\n");
        }
        outputWriter.flush();
        outputWriter.close();
    }
	
	public static int[] countingSort(int[] a, int range) {
		int[] c = new int[range + 1]; // provides temporary working storage
		int[] b = new int[a.length];
		
		for (int i = 0; i < a.length; i++) {
			c[a[i]]++;
		}
		for (int i = 1; i < c.length; i++) {
			c[i] += c[i - 1];
		}
		
		for (int i = a.length - 1; i >= 0; i--) {
			c[a[i]]--;
			b[c[a[i]]] = a[i];
		}
		return b;
	}
	
	private static void swap(int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}
