package lab2s2;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class Lab2 {
	private static final int N = 6500000;
	public static void main (String[] args) {
		/*int[] arr = new int[N];
		Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(N);
        }*/
        
        String num = String.valueOf(N);
        String filename = "input"+num+".txt";
        
        /*try {
        	writeArray(filename, arr);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        //String filename = "input"+num+".txt";
        
        int[] arr = readArray("reverse.txt", N);
        
		double before = System.currentTimeMillis();
		
		mergesort(arr,0,N-1);

		double after = System.currentTimeMillis();
		
		
		System.out.print("Time of sorting:");
		double res = after-before;
		System.out.print(res);
		//TestReverseAndSortedArr();
		
	}
	
    private static int[] readArray(String file, int n){
        try{
            String buff;
            Scanner s = new Scanner(new File(file));
            int[] array = new int[n];
            for (int i = 0; i < n; i++){
                buff = s.next();
                array[i] = Integer.parseInt(buff);
            }
            s.close();
            return array;
        }
        catch (FileNotFoundException e){

        }
        return null;
    }
	
	public static void writeArray (String filename, int[] arr) throws IOException{
        BufferedWriter outputWriter = null;
        outputWriter = new BufferedWriter(new FileWriter(filename));
        for (int i = 0; i < arr.length; i++) {
            outputWriter.write(arr[i]+"\n");
        }
        outputWriter.flush();
        outputWriter.close();
    }
	
	
	private static void mergesort(int a[], int l, int r) {
		if (l < r) {
			int m = (l + r) / 2;
			mergesort(a, l, m);
			mergesort(a, m + 1, r);
			merge(a, l, m, r);
		}
	}

	public static void merge(int arr[], int l, int m, int r) {
		int n1 = m - l + 1;
		int n2 = r - m;

		int[] leftArray = new int[n1];
		int[] rightArray = new int[n2];

		// copy from left subarray
		for (int i = 0; i < leftArray.length; i++)
			leftArray[i] = arr[l + i];
		// copy from right subarray
		for (int j = 0; j < rightArray.length; j++)
			rightArray[j] = arr[m + j + 1];

		for (int k = l, i = 0, j = 0; k <= r; k++) {

			if (i == leftArray.length) {
				arr[k] = rightArray[j++];
				continue;
			}
			if (j == rightArray.length) {
				arr[k] = leftArray[i++];
				continue;
			}

			if (leftArray[i] <= rightArray[j]) {
				arr[k] = leftArray[i++];
			} else {
				arr[k] = rightArray[j++];
			}
		}

	}
	
	private static void swap(int[] studs, int i, int j) {
		int temp = studs[i];
		studs[i] = studs[j];
		studs[j] = temp;
	}
	
	private static void TestReverseAndSortedArr() {
		int[] arr = new int[N];
		Random rand = new Random();
        for (int i = 0; i < N; i++) {
            arr[i] = rand.nextInt(N);
        }
        
		double before = System.currentTimeMillis();
		
		mergesort(arr,0,N-1);

		double after = System.currentTimeMillis();
		
		
		System.out.print("Time of random sorting:");
		double res = after-before;
		System.out.print(res);
		
		
		double before1 = System.currentTimeMillis();
		
		mergesort(arr,0,N-1);

		double after1 = System.currentTimeMillis();
		
		
		System.out.print("\nTime of sorting of sorted array:");
		double res1 = after1-before1;
		System.out.print(res1);
		
		
		for (int i = 0; i < N/2; i++) {
	            swap(arr,i,N-1-i);
	    }
		 
		double before2 = System.currentTimeMillis();
			
		mergesort(arr,0,N-1);

		double after2 = System.currentTimeMillis();
			
			
		System.out.print("\nTime of reverse sorting:");
		double res2 = after2-before2;
		System.out.print(res2);
	}
}
