package task1;

public class Task1 {
	public static void main(String[] args) {
		//int[] array1 = {};
		//int[] array1 = {10,5};
		//int[] array1 = {5,-5};
		int[] array1 = {10,-55,8,-5,7};
		//int[] array1 = {10,-55,8,-5,-55,7};
		
		if (array1.length != 0) {
			
			System.out.println("array before swap");
			for (int i = 0; i < array1.length; i++) {
				System.out.printf("%-7d", array1[i]);
			}
			
			int min = 0;
			int posMin = 0, posMax = 0;
			boolean flagMin = false, flagMax = false;
			for (int i = 0; i < array1.length; i++) {
				if (array1[i] < min) {
					min = array1[i];
					posMin = i;
					flagMin = true;
				}
			}
			
			int max = min;
			for (int i = 0; i < array1.length; i++) {
				if (array1[i] > max && array1[i] < 0) {
					max = array1[i];
					posMax = i;
					flagMax = true;
				}
			}
			
			if (flagMin == true && flagMax == true) {
				int temp = min;
				min = max;
				max = temp;
				array1[posMin] = min;
				array1[posMax] = max;
			}
			
			
			System.out.printf("\n array after swap\n");
			for (int i = 0; i < array1.length; i++) {
				System.out.printf("%-7d", array1[i]);
			}
		} else {
			System.out.println("Error");
		}
	}
}
