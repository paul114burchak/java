package task2;

import java.io.File;

public class Task2 {
	private static String fileName = "input.dat";
	private static String currenDir = System.getProperty("user.dir") + File.separatorChar + "data";

	private static DLNode firstList = null;
	public static void main(String[] args) {
		FileAssistant fa = new FileAssistant();

		fa.setFileName(currenDir, fileName);
		DLNode firstList = null;

		// Task 3.1: Create the first double-linked list

		if (fa.readFile()) {
			// Successful reading file

			System.out.println("Start reading a file: ");

			// read the first integer value from the text file
			int number = fa.readNextInt();

			while (number != FileAssistant.ERROR_CODE) {
				//Output integer from a file
				System.out.printf("%5d", number);

				//create new node of double-linked list
				DLNode node = createDLNode(number);
				
				//add new node to a double-linked list
				firstList = addNode(firstList, node);

				// read the next integer value from the text file
				number = fa.readNextInt();
			}
			
			System.out.println("\nStop reading a file: \n");

		} else {
			System.out.println("Error: file empty, or does't not found, or there are IO errors: \n");
		}
		int a = 5;
		System.out.println(System.lineSeparator());
		System.out.println("THE FIRST LIST:");
		printList(firstList);

		// Task 3.2:Change the first double-linked list and create new single-linked list
		SLNode secondList = createSecondList(firstList,a);

		// print the second list
		System.out.println("THE SECOND LIST:");
		printList(secondList);

		// check the content of the first list	
		System.out.println("THE FIRST LIST:");
		printList(firstList);
	}
	
	
	private static SLNode createSLNode(int data) {
		SLNode newNode = new SLNode();
		newNode.data = data;
		newNode.next = null;
		return newNode;

	}


	private static DLNode createDLNode(int data) {
		DLNode newNode = new DLNode();
		newNode.data = data;
		newNode.next = null;
		newNode.prev = null;
		return newNode;

	}
	
	private static void printList(SLNode list) {
		SLNode x = list;
		if (x == null)
		{
		    System.out.printf("The list is empty!\n");
		}
		else
		{
		    while (x != null)
		    {
			System.out.printf("%5d", x.data);
			x = x.next;
		    }
		    System.out.printf("\n");
		}
	}
	


	private static void printList(DLNode list) {
		DLNode x = list;
		if (x == null)
		{
		    System.out.printf("The list is empty!\n");
		}
		else
		{
		    while (x != null)
		    {
			System.out.printf("%5d", x.data);
			x = x.next;
		    }
		    System.out.printf("\n");
		}
	}

	
	private static SLNode createSecondList(DLNode dlHead, int a) {
		SLNode head = null;
        DLNode x = dlHead; 
        while (x != null) {
        	if (x.data < a) {
        		dlHead = remove(dlHead,x);
        		if (head == null) {
        			head = addFirst(head,x.data);
        		} else {
        			SLNode node = createSLNode(x.data);
        			head = addNode(head,node);
        		}
        	}
        	x = x.next;
        }
		return head;
	}
	
	private static DLNode addNode(DLNode head, DLNode node) {
		if (null == head){
			head = node;
		} else {
			DLNode tail = head;
		    while(tail.next != null)
			tail = tail.next;
		    addAfter(tail, node);
		}
		return head;
	}
	

	private static SLNode addNode(SLNode head, SLNode node) {
		addAfter(head,node);
		return head;
	}
	
	private static void addAfter (DLNode y, DLNode x){
	    x.next = y.next;
	    x.prev = y;
	    if(y.next != null) {
	    	y.next.prev = x;
	    }
	    y.next = x;
	}
	
	private static DLNode remove(DLNode h, DLNode x) {
		if (x.prev != null) {
			x.prev.next = x.next;
		} else {
			h = h.next;
			firstList = h;
		}
		if (x.next != null) {
			x.next.prev = x.prev;
		}
		return h;
	}
	
	private static void addAfter(SLNode x, SLNode y) {
		y.next = x.next;
		x.next = y;
	}
	
	private static SLNode addFirst(SLNode head, int value) {
	        SLNode x = createSLNode(value);
	        x.data = value;
	        x.next = head;
	        return x;   
	}
}
