package bst;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 5
 * 
 * @author Olena Khomenko <br>
 *         Represents information about student: its name and number of course <br>
 *         This class is a sample how to define class, fields and methods
 * 
 *         Rewrite this class and its methods <br>
 *         Choose information to be saved in a class from lab manuals (table 1,
 *         col.2).<br>
 * 
 *         Write methods setXXX to set specified value to the field XXX. <br>
 * 
 *         Write method print to output information about student (values of the
 *         fields) in formatted string. <br>
 * 
 *         Write static methods boolean isValidXXX to check whether specified
 *         string could set (or convert and set) to the field XXX
 *
 */

public class Student {
	private String surname;
	private String name;
	private int course;
	private int cardNumber;
	private boolean isServeInMilitary;

	// TODO
	// add fields according to your variant

	public Student(int cardNumber, String surname, String name, int course, boolean isServedInMilitary) {
		this.cardNumber = cardNumber;
		this.surname = surname;
		this.name = name;
		this.course = course;
		this.isServeInMilitary = isServedInMilitary;
	}

	public int getKey() {return cardNumber;}
	public int getCourse() {return course;}
	public String getName() {return name;}
	public String getSurname() {return surname;}
	public boolean getMilitary() {return isServeInMilitary;}
	public void setKey(int key) {this.cardNumber = key;}
	
	@Override
	public String toString() {
		return String.format("%-7d|%-10s|%-10s|	%d| %-5b|", cardNumber,surname,name,course,isServeInMilitary);
	}

	/**
	 * Determines if the specified string is valid card number: contains only
	 * one digit character
	 * 
	 * @param number
	 *            the string to be tested
	 * @return true if the specified string is a valid card number, false
	 *         otherwise.
	 */
	public static boolean isValidCardNumber(String number) {
		char[] chArray = number.toCharArray();
		for (char ch : chArray) {
			if (!Character.isDigit(ch)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isValidCourse(String number) {
		if (number.charAt(0)-'0' >= 1 && number.charAt(0)-'0' <= 6 && number.length() == 1)
		return true;
		return false;
	}

	public static boolean isValidSurname(String letters) {
		char[] chArray = letters.toCharArray();
		for (char ch : chArray) {
			if (Character.isDigit(ch)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isValidName(String letters) {
		char[] chArray = letters.toCharArray();
		for (char ch : chArray) {
			if (Character.isDigit(ch)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isValidMilitary(String bool) {
		if (bool.equals("true") || bool.equals("false"))
		return true;
		return false;
	}
}
