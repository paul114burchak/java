package bst;

import java.io.File;
import java.util.List;

public class Task1Main {
	// name of file which stores data about students
	private static String fileName = "students1.csv";

	// path to the current directory that contains directory "data"
	private static String currenDir = System.getProperty("user.dir")
			+ File.separatorChar + "data";

	public static void main(String[] args) {
		// create object of class StudentReader and invoke method read
		// TODO
		StudentReader reader = new StudentReader(fileName);
		List<Student> students = reader.read();
		StudentDictionary<Student> dict = new BSTree<>();

		// TEST dict.put
		// fill dictionary:insert students to the dictionary
		// TODO
		for (int i = 0; i < students.size(); i++) {
			dict.put(students.get(i));
		}
		// TEST dict.print dictionary
		// output the content of a dictionary
		// TODO
		
		dict.printDictionary();
		System.out.println("___________________________________________");
		// TEST dict.put
		// insert students with non-unique key
		// TODO	
		Student nonUnique = new Student(1413,"sadr","seriy",6,true);
		System.out.println(dict.put(nonUnique));


		System.out.println("___________________________________________");
		dict.printDictionary();
		// TEST: dict.contains
		// TODO
		if (dict.containsKey(nonUnique.getKey())) System.out.println("contains");
		else System.out.println("don't contains");

		if (dict.containsKey(1200)) System.out.println("contains");
		else System.out.println("don't contains");

		System.out.println("___________________________________________");
		// TEST: dict.get
		// TODO

		System.out.println(dict.get(1413).toString());
		System.out.println(dict.get(1923).toString());
		System.out.println("___________________________________________");
		// TEST: dict.remove
		// TODO
		dict.remove(1413);
		dict.remove(1553);
		dict.remove(1390);
		dict.remove(1744);
		dict.remove(1337);
		dict.printDictionary();
		System.out.println("___________________________________________");
		
		Student t1 = new Student(1700,"sadr","seriy",6,true);
		Student t2 = new Student(1680,"rrr","ppp",5,true);
		Student t3 = new Student(1690,"eee","yyy",1,false);
		Student t4 = new Student(1750,"www","kkk",2,true);
		dict.put(t1);	
		dict.put(t2);
		dict.put(t3);
		dict.put(t4);
		
		dict.remove(5,true);
		dict.printDictionary();
	}

}
