package lab2;

import java.util.Random;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (1) Laboratory work 2
 * 
 * @author Olena Khomenko This is a program skeleton for lab 2 Write your code
 *         in the places of the program which are marked by TODO marker
 * 
 *         The size of array1 is 5 elements In order to test a program on
 *         different sizes you need to change N Test cases are: array1.lenght >=
 *         20 - Elements are unique. Searching is successful array1.lenght >= 20
 *         - Elements are not unique. Searching is successful array1.lenght = 0
 *         array1.lenght = 2 - Searching is successful/non-successful
 *         array1.lenght = 3 - Searching is successful/non-successful Generate
 *         specific test data to test your program
 */

public class Main {
	private static final int N1 = 5; // amount of elements in the array1

	public static void main(String[] args) {

		// define array1
		int[] array1 = new int[N1];

		// fill the array1 by random values in range [0;49]
		Random rand = new Random();
		for (int i = 0; i < array1.length; i++) {
			array1[i] = rand.nextInt(100)- 50;
		}

		// output array1
		System.out.println("\tTASK 2.1\nArray1:");
		for (int i = 0; i < array1.length; i++) {
			System.out.printf("%-7d", array1[i]);
		}
		System.out.println();

		// TODO
		// task 2.1
		// see the task from table, column 2

		// for example, pos = 3, range = [pos; N-1]
		
		int posMin0 = -1;
		
		for (int i = 0, min = 0; i < array1.length; i++) {
			if (array1[i] < min) {
				min = array1[i];
				posMin0 = i;
			}
		}

		
		// TODO
		// print the results of task 2.1
		
		if (posMin0 > -1) {
			System.out.printf("Pos.of min among negative numbers of array1: %d\n\n", posMin0);
		} else {
			System.out.println("Pos.of min among negative numbers of array1 doesn't exist\n");
		}
		
		System.out.println("\tTesting:");
		
		// Testing 1
		
		int[] array1_1 = new int[20];
		int posMin1 = -1;
		
		System.out.println("Array1_1(Length = 20, Elements are unique):");
		for (int i = 0, min = 0; i < array1_1.length; i++) {
			array1_1[i] = rand.nextInt(1000)- 500;
			System.out.printf("%-7d", array1_1[i]);
			
			if (array1_1[i] < min) {
				min = array1_1[i];
				posMin1 = i;
			}
		}
		
		if (posMin1 > -1) {
			System.out.printf("\nPos.of min among negative numbers of array1_1: %d\n\n", posMin1);
		} else {
			System.out.println("\nPos.of min among negative numbers of array1_1 doesn't exist\n");
		}
		
		// Testing 2
		
		int[] array1_2 = new int[20];
		int posMin2 = -1;
		
		System.out.println("Array1_2(Length = 20; Elements aren't unique):");
		for (int i = 0, min = 0; i < array1_2.length; i++) {
			array1_2[i] = rand.nextInt(10)- 5;
			System.out.printf("%-7d", array1_2[i]);
			
			if (array1_2[i] < min) {
				min = array1_2[i];
				posMin2 = i;
			}
		}
		
		if (posMin2 > -1) {
			System.out.printf("\nPos.of min among negative numbers of array1_2: %d\n\n", posMin2);
		} else {
			System.out.println("\nPos.of min among negative numbers of array1_2 doesn't exist\n");
		}
		
		// Testing 3
		
		int[] array1_3 = new int[0];
		int posMin3 = -1;  
		 
		System.out.println("Array1_3(Length = 0):"); 
		for (int i = 0, min = 0; i < array1_3.length; i++) { 
			array1_3[i] = rand.nextInt(10)- 5; 
			System.out.printf("%-7d", array1_3[i]); 
			 
			if (array1_3[i] < min) { 
				min = array1_3[i]; 
				posMin3 = i; 
			} 
		} 
		
		if (posMin3 > -1) {
			System.out.printf("Pos.of min among negative numbers of array1_3: %d\n\n", posMin3);
		} else {
			System.out.println("Pos.of min among negative numbers of array1_3 doesn't exist\n");
		}
		
		// Testing 4 
		
		int[] array1_4 = new int[2];
		int posMin4 = -1;  
				 
		System.out.println("Array1_4(Length = 2):"); 
		for (int i = 0, min = 0; i < array1_4.length; i++) { 
			array1_4[i] = rand.nextInt(7)- 3; 
			System.out.printf("%-7d", array1_4[i]); 
					 
			if (array1_4[i] < min) { 
				min = array1_4[i]; 
				posMin4 = i; 
			} 
		}  
		
		if (posMin4 > -1) {			
			System.out.printf("\nPos.of min among negative numbers of array1_4: %d\n\n", posMin4);
		} else {
			System.out.println("\nPos.of min among negative numbers of array1_4 doesn't exist\n");
		}
		
		// Testing 5 
		
		int[] array1_5 = {5,8,5};
		int posMin5 = -1;  
				
		System.out.println("Array1_5(Length = 3):"); 
		for (int i = 0, min = 0; i < array1_5.length; i++) { 
			System.out.printf("%-7d", array1_5[i]); 
							 
			if (array1_5[i] < min) { 
				min = array1_5[i]; 
				posMin5 = i; 
			} 
		}  
				
		if (posMin5 > -1) {			
			System.out.printf("\nPos.of min among negative numbers of array1_5: %d\n\n", posMin5);
		} else {
			System.out.println("\nPos.of min among negative numbers of array1_5 doesn't exist");
		}
				
		// Testing 5 
		
		int[] array1_6 = {-2,8,4,12,0};
		int posMin6 = -1;  
						
		System.out.println("Array1_6(Length = 5):"); 
		for (int i = 0, min = 0; i < array1_6.length; i++) { 
			System.out.printf("%-7d", array1_6[i]); 
									 
			if (array1_6[i] < min) { 
				min = array1_6[i]; 
				posMin6 = i; 
			} 
		}  
						
		if (posMin6 > -1) {			
			System.out.printf("\nPos.of min among negative numbers of array1_6: %d\n\n", posMin6);
		} else {
			System.out.println("\nPos.of min among negative numbers of array1_6 doesn't exist");
		}
		// TODO 
		// task 2.2
		// see the task from table, column 3
		System.out.println("\tTASK 2.2");
		// calculate the size of array2
		// sample: range from pos up to N-1
		
		int N2 = (array1.length - 1) - posMin0; 	
		// create an array2 by size N2
		int[] array2 = new int[N2];
		// copy elements from array1 to array2 
		
		if (posMin0 > -1) {
	
			for (int i = array1.length-1, j = 0; i > posMin0; i--, j++) { 
				array2[j] = array1[i]; 
			} 
 
		// TODO
		// output the array2 
			
			System.out.println("Array2:");
			
			for (int j = 0; j < array2.length; j++) { 
				System.out.printf("%-7d", array2[j]);
			} 
			System.out.println();
		} else {
			System.out.println("Array2 doesn't exist");
		}
		
		// TODO
		// task 2.3 
		// see the task from table, column 4  
		
		System.out.println("\n\tTASK 2.3");
		
		if (posMin0 > -1) {
			int count = 0;
			int sum = 0;
			for (int j = 0; j < array2.length;j++) {
				if (Math.abs(array2[j]) % 2 != 0) {
					sum += array2[j];
					count++;
				}
			}
			
		// TODO
		// print the results of task 2.3

			if(count != 0) {
				System.out.printf("Sum of not even numbers: %d\n",sum);
			} else {
				System.out.println("Not even numbers don't exists, sum can't be computed");
			}
		} else {
			System.out.println("Sum of not even numbers doesn't exist, because Array2 doesn't exist");
		}
		
		
		System.out.println("\n\tTesting:");
		// Testing 1
		
		int N21 = (array1_1.length - 1) - posMin1;
		int[] array2_1 = new int[N21];
		
		if (posMin1 > -1) {
			for (int i = array1_1.length-1, j = 0; i > posMin1; i--, j++) { 
				array2_1[j] = array1_1[i]; 
			} 
			System.out.println("\nArray2_1:");
			
			int count = 0;
			int sum = 0;
			for (int j = 0; j < array2_1.length; j++) { 
				System.out.printf("%-7d", array2_1[j]);
				if (Math.abs(array2_1[j]) % 2 != 0) {
					sum += array2_1[j];
					count++;
				}
			} 
			System.out.println();
			
			if(count != 0) {
				System.out.printf("Sum of not even numbers: %d\n",sum);
			} else {
				System.out.println("Not even numbers don't exists, sum can't be computed");
			}
		} else {
			System.out.println("Array2_1 doesn't exist");
			System.out.println("Sum of not even numbers doesn't exist, because Array2_1 doesn't exist");
		}
		
		// Testing 2
		
		int N22 = (array1_2.length - 1) - posMin2;
		int[] array2_2 = new int[N22];
		
		if (posMin2 > -1) {
			for (int i = array1_2.length-1, j = 0; i > posMin2; i--, j++) { 
				array2_2[j] = array1_2[i]; 
			} 
			System.out.println("\nArray2_2:");
				
			int count = 0;
			int sum = 0;
			for (int j = 0; j < array2_2.length; j++) { 
				System.out.printf("%-7d", array2_2[j]);
				if (Math.abs(array2_2[j]) % 2 != 0) {
					sum += array2_2[j];
					count++;
				}
			} 
			System.out.println();
					
			if(count != 0) {
				System.out.printf("Sum of not even numbers: %d\n",sum);
			} else {
				System.out.println("Not even numbers don't exists, sum can't be computed");
			}
		} else {
			System.out.println("Array2_2 doesn't exist");
			System.out.println("Sum of not even numbers doesn't exist, because Array2_2 doesn't exist");
		}
	
		// Testing 3
		
		int N23 = (array1_3.length - 1) - posMin3;
		int[] array2_3 = new int[N23];
	
		if (posMin3 > -1) {
			for (int i = array1_3.length-1, j = 0; i > posMin3; i--, j++) { 
				array2_3[j] = array1_3[i]; 
			} 
			System.out.println("\nArray2_3:");
				
			int count = 0;
			int sum = 0;
			for (int j = 0; j < array2_3.length; j++) { 
				System.out.printf("%-7d", array2_3[j]);
				if (Math.abs(array2_3[j]) % 2 != 0) {
					sum += array2_3[j];
					count++;
				}
			} 
			System.out.println();
					
			if(count != 0) {
				System.out.printf("Sum of not even numbers: %d\n",sum);
			} else {
				System.out.println("Not even numbers don't exists, sum can't be computed");
			}
		} else {
			System.out.println("\nArray2_3 doesn't exist");
			System.out.println("Sum of not even numbers doesn't exist, because Array2_3 doesn't exist");
		}
		
		// Testing 4
		
		int N24 = (array1_4.length - 1) - posMin4;
		int[] array2_4 = new int[N24];
	
		if (posMin4 > -1) {
			for (int i = array1_4.length-1, j = 0; i > posMin4; i--, j++) { 
				array2_4[j] = array1_4[i]; 
			} 
			System.out.println("\nArray2_4:");
				
			int count = 0;
			int sum = 0;
			for (int j = 0; j < array2_4.length; j++) { 
				System.out.printf("%-7d", array2_4[j]);
				if (Math.abs(array2_4[j]) % 2 != 0) {
					sum += array2_4[j];
					count++;
				}
			} 
			System.out.println();
					
			if(count != 0) {
				System.out.printf("Sum of not even numbers: %d\n",sum);
			} else {
				System.out.println("Not even numbers don't exists, sum can't be computed");
			}
		} else {
			System.out.println("\nArray2_4 doesn't exist");
			System.out.println("Sum of not even numbers doesn't exist, because Array2_4 doesn't exist");
		}
		
		// Testing 5
		
		int N25 = (array1_5.length - 1) - posMin5;
		int[] array2_5 = new int[N25];
	
		if (posMin5 > -1) {
			for (int i = array1_5.length-1, j = 0; i > posMin5; i--, j++) { 
				array2_5[j] = array1_5[i]; 
			} 
			System.out.println("\nArray2_5:");
				
			int count = 0;
			int sum = 0;
			for (int j = 0; j < array2_5.length; j++) { 
				System.out.printf("%-7d", array2_5[j]);
				if (Math.abs(array2_5[j]) % 2 != 0) {
					sum += array2_5[j];
					count++;
				}
			} 
			System.out.println();
					
			if(count != 0) {
				System.out.printf("Sum of not even numbers: %d\n",sum);
			} else {
				System.out.println("Not even numbers don't exists, sum can't be computed");
			}
		} else {
			System.out.println("\nArray2_5 doesn't exist");
			System.out.println("Sum of not even numbers doesn't exist, because Array2_5 doesn't exist");
		}
		
		// Testing 6
		
		int N26 = (array1_6.length - 1) - posMin6;
		int[] array2_6 = new int[N26];
	
		if (posMin6 > -1) {
			for (int i = array1_6.length-1, j = 0; i > posMin6; i--, j++) { 
				array2_6[j] = array1_6[i]; 
			} 
			System.out.println("\nArray2_6:");
				
			int count = 0;
			int sum = 0;
			for (int j = 0; j < array2_6.length; j++) { 
				System.out.printf("%-7d", array2_6[j]);
				if (Math.abs(array2_6[j]) % 2 != 0) {
					sum += array2_6[j];
					count++;
				}
			} 
			System.out.println();
					
			if(count != 0) {
				System.out.printf("Sum of not even numbers: %d\n",sum);
			} else {
				System.out.println("Not even numbers don't exists, sum can't be computed");
			}
		} else {
			System.out.println("Array2_6 doesn't exist");
			System.out.println("Sum of not even numbers doesn't exist, because Array2_6 doesn't exist");
		}
	} 
		
	
} 
