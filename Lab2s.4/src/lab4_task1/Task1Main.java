package lab4_task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;
import common.FigureSet;
import common.GeomFigure;

public class Task1Main {
	// name of file which stores data about geometric figures
	private static String fileName = "figures.csv";

	// path to the current directory that contains directory "data"
	private static String currenDir = System.getProperty("user.dir")
			+ File.separatorChar + "data";

	public static void main(String[] args) {
		// 1) read all lines from a file

		List<String[]> lines = readcsvFile(fileName, currenDir);

		if (!lines.isEmpty()) {
			int numLines = lines.size();
			System.out.println("File contains " + numLines + "  lines");

			// 2) create the array of geometric figures and check data from the
			// file
			GeomFigure[] farray = createArrayOfFigures(lines);

			// 3) Create a set of figures
			FigureSet figures = new Hastable(10);
			for (GeomFigure f : farray) {
				figures.add(f);
			}

			// 4) Print a set of figures
			figures.print();

		} else {
			System.out.println("Error: file  " + fileName + "   is empty");
			System.exit(0);
		}

	}

	static List<String[]> readcsvFile(String fileName, String dirName) {
		CSVReader reader = null;
		GeomFigure[] figures = null;
		String path = Paths.get(currenDir, fileName).toString();
		List<String[]> list = null;

		// read data from a file
		try {

			reader = new CSVReader(new FileReader(path));
			System.out.println("File \"" + path + " \"  have been reading ");

			// read all lines from a file
			list = reader.readAll();
			reader.close();

		} catch (FileNotFoundException e) {
			System.out.println("Error: file  " + path + "   not found");
		} catch (IOException e) {
			System.err.println("Error:read file " + path + "   error");
		} catch (SecurityException e) {
			System.err.println(e.getMessage());
		}

		return list;
	}

	static GeomFigure[] createArrayOfFigures(List<String[]> list) {
		// TODO
		int num = 0;

		// create empty array of geometric figures with the length = list.size()
		GeomFigure[] farray = new GeomFigure[list.size()];

		// start reading and analyzing each line from this list
		for (Iterator<String[]> it = list.iterator(); it.hasNext();) {

			// line contains data about one geometric figures
			String[] line = it.next();

			try {
				// TODO
				// check information about geometric figure and fill
				// fields of GeomFigure object
				int coords[] = new int[4];
				for (int i = 0; i < 4; i++) 
					coords[i] = Integer.valueOf(line[i]);
				farray[num++] = new GeomFigure(coords);

			} catch (NumberFormatException e) {
			}

		}
		// check if all data in a file are valid
		if (num != list.size()) {

			// if not, create new array with smaller length
			farray = copyOf(farray, num);
		}

		return farray;
	}

	static GeomFigure[] copyOf(GeomFigure[] farray, int num) {
		// TODO
		// Create new array of geometric figures of size num
		// Copy num first elements from farray students to the new array
		// Return new array
		GeomFigure[] CopyArray = new GeomFigure[num];
		
		for (int i = 0; i < num; ++i) {
			CopyArray[i] = farray[i];
		}
		return CopyArray;
	}

}
