package lab4_task1;

import java.util.Arrays;

import common.FigureSet;
import common.GeomFigure;

/**
 * 
 * This class implements a set as a hash table. Hash table is an associative
 * array of entries. <br>
 * Each entry contains geometric figure or null. <br>
 * Hash table does not resolve any collisions
 *
 */
public class Hastable implements FigureSet {

	private GeomFigure[] table;

	/**
	 * the number of non-null entries in the hashtable
	 */
	private int size;

	/**
	 * default size of the hashtable
	 */
	private final int initialCapacity = 11;

	/**
	 * The load factor is a measure of how full the hash table is allowed to get
	 * / before its capacity is automatically increased
	 */
	private double loadFactor = 0.75;

	/**
	 * It is used in multiplication hash function
	 */
	private final double A = (Math.sqrt(5) - 1) / 2;

	private int capacity;
	/**
	 * Constructs a new, empty hashtable with a default initial capacity (11)
	 * and load factor (0.75).
	 */
	public Hastable() {

		// TODO
		// create an array of size equals to default initialCapacity
		this.size = 0;
		this.capacity = initialCapacity;
		this.table = new GeomFigure[initialCapacity];

	}

	/**
	 * Constructs a new, empty hashtable with the specified initial capacity and
	 * default load factor (0.75).
	 * 
	 * @param initialCapacity
	 *            the initial capacity of the hashtable
	 */
	public Hastable(int initialCapacity) {

		// TODO
		// create an array of size equals to initialCapacity
		this.table = new GeomFigure[initialCapacity];
		this.size = 0;
		this.capacity = initialCapacity;
	}

	/**
	 * Constructs a new, empty hashtable with the specified initial capacity and
	 * the specified load factor.
	 * 
	 * @param initialCapacity
	 *            the initial capacity of the hashtable
	 * @param loadFactor
	 *            the load factor of the hashtable
	 */
	public Hastable(int initialCapacity, double loadFactor) {

		// TODO create an array of size equals to initialCapacity
		// TODO initialize field loadFactor
		this.table = new GeomFigure[initialCapacity];
		this.loadFactor = loadFactor;
		this.size = 0;
		this.capacity = initialCapacity;
		
	}

	/**
	 * Returns the number of entries in the hashtable
	 * 
	 * @return the number of entries in the hashtable
	 */
	public int size() {

		return size;
	}

	/**
	 * Increases the capacity of and internally reorganizes this hashtable, in
	 * order to accommodate and access its entries more efficiently. This method
	 * is called when the number of elements in the hashtable exceeds this
	 * hashtable's capacity and load factor
	 */
	private void rehash() {
		// TODO
		GeomFigure[] oldTable = this.table;
		this.capacity += 16;
		this.table = new GeomFigure[capacity];
		for (int i = 0; i < oldTable.length; i++)
			add(oldTable[i]);
	}

	/**
	 * The hash function is used to calculate the hasvalue of the object gf.
	 * Choose hashing method from your variant (table 1): deletion or
	 * multiplication
	 * 
	 * @param gf
	 *            some geometric figure
	 * @return hash value - index in table
	 */
	private int hash(GeomFigure gf) {
		// TODO
		//
		return Math.abs(gf.hashCode()) % this.capacity;
	}

	@Override
	public boolean add(GeomFigure gf) {
		// TODO
		// if gf is not null
		// ---calculate number of slot where should be object gf
		// ------if slot is empty
		// ----------write gf to the slot and rehash if need
		// ----------increase the size of hash table
		if (gf != null) {
			int index = hash(gf);
			if (table[index] == null) {
				table[index] = gf;
				if ((double)this.size/this.capacity >= this.loadFactor) rehash();
				size++;
				return true;
			}
		}
		return false;
		
	}

	@Override
	public boolean contains(GeomFigure gf) {
		// TODO
		// if gf is not null
		// ----Calculate hashvalue of gf
		// ------if slot is not empty
		// ---------compare gf and object from the slot
		if (gf != null){
			int index = hash(gf);
			if(table[index] != null && gf.equals(table[index]))
				return true;
		}
		return false;
	}

	@Override
	public boolean remove(GeomFigure gf) {
		// TODO
		// if gf is not null, return false
		// ---Calculate number of slot where should be object gf
		// ---if slot is not empty
		// -------write null into the slot
		// -------decrease the size of hash table
		// -------return true
		if (gf != null) {
			int index = hash(gf);
			if (table[index] != null) {
				table[index] = null;
				size--;
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO
		// check the size of hashtable
		if(size == 0) return true;
		else return false;
	}

	@Override
	public void print() {
		// TODO
		// Output the table , where each row contains a number of the hash-table
		// slot, and the element itself.For example,

		// System.out.println(String.format(" %-6d|%s", index, table [index]));

		// If a slot is empty the row contains a number of the hash-table slot
		// and the message �The slot is empty�.
		for (int i = 0; i < capacity; i++) {
			if (this.table[i] == null) System.out.printf("%d |The slot is empty\n",i);
			else {
				System.out.printf("%d | %d | %d|  ",i, this.table[i].getSquare(), this.table[i].getPerimeter());
				this.table[i].printCoordinates();
			}
		}
	}

}
