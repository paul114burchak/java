package lab4_task2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;

import org.junit.Test;

import common.FigureSet;
import common.GeomFigure;
//import lab4_task1.Hastable;
import lab4_task1.Hastable;

public class TestTask2 {

	@Test
	public void testEmptyTable() {
		OAHastable tableEmpty = new OAHastable(3);
		assertEquals(tableEmpty.size(), 0);
		assertTrue(tableEmpty.isEmpty());

		// remove from empty hashTable
		int coords[] = {3,0,5,7};
		GeomFigure gf = new GeomFigure(coords);
		assertFalse(tableEmpty.remove(gf));
		assertEquals(tableEmpty.size(), 0);

		// search in empty hashTable
		assertFalse(tableEmpty.contains(gf));

	}

	@Test
	public void testAddOneElement() {
		// TODO
		// add one element, check the size

		FigureSet table = new OAHastable(3);
		int coords[] = {3,0,5,7};
		GeomFigure gf = new GeomFigure(coords);
		assertTrue(table.add(gf));
		assertEquals(table.size(),1);
	}

	@Test
	public void testAddSomeUniqueElements() {
		// TODO
		FigureSet table = new OAHastable(3);
		int coords1[] = {3,0,5,7};
		GeomFigure gf1 = new GeomFigure(coords1);
		int coords2[] = {32,3,5,74};
		GeomFigure gf2 = new GeomFigure(coords2);
		assertTrue(table.add(gf1));
		assertTrue(table.add(gf2));
		assertFalse(gf1.equals(gf2));
	}

	@Test
	public void testAddSomeNotUniqueElements() {
		// TODO
		FigureSet table = new OAHastable(10);
		int coords1[] = {3,0,5,7};
		GeomFigure gf1 = new GeomFigure(coords1);
		int coords2[] = {3,0,5,7};
		GeomFigure gf2 = new GeomFigure(coords2);
		assertTrue(table.add(gf1));
		assertFalse(table.add(gf2));
		assertTrue(gf1.equals(gf2));
	}

	@Test
	public void testRehash() {
		// TODO
		FigureSet table = new OAHastable(3,0.5);
		int coords1[] = {3,0,5,9};
		GeomFigure gf1 = new GeomFigure(coords1);
		int coords2[] = {81,2,53,7};
		GeomFigure gf2 = new GeomFigure(coords2);
		int coords3[] = {42,0,5,7};
		GeomFigure gf3 = new GeomFigure(coords3);
		int coords4[] = {42,9,442,7};
		GeomFigure gf4 = new GeomFigure(coords4);
		assertTrue(table.add(gf1));
		assertTrue(table.add(gf2));
		assertTrue(table.add(gf3));
		assertTrue(table.add(gf4));
	}

	@Test
	public void testRemove() {
		// TODO
		FigureSet table = new OAHastable(10);
		int coords[] = {3,0,5,7};
		GeomFigure gf = new GeomFigure(coords);
		assertFalse(table.remove(gf));
		int coords1[] = {3,0,5,9};
		GeomFigure gf1 = new GeomFigure(coords1);
		int coords2[] = {3,0,5,7};
		GeomFigure gf2 = new GeomFigure(coords2);
		int coords3[] = {42,0,442,7};
		GeomFigure gf3 = new GeomFigure(coords3);
		int coords4[] = {42,0,5,7};
		GeomFigure gf4 = new GeomFigure(coords4);
		assertTrue(table.add(gf1));
		assertTrue(table.add(gf2));
		assertTrue(table.add(gf3));
		assertTrue(table.contains(gf3));
		assertTrue(table.remove(gf3));
		assertFalse(table.remove(gf3));
		assertFalse(table.remove(gf4));
	}


	@Test
	public void testRemove2() {	
		FigureSet table = new Hastable(7);
		int coords1[] = {3,0,5,7};
		GeomFigure gf1 = new GeomFigure(coords1);
		int coords2[] = {3,24,4,8};
		GeomFigure gf2 = new GeomFigure(coords2);
		int coords3[] = {11,24,10,92};
		GeomFigure gf3 = new GeomFigure(coords3);
		assertTrue(table.add(gf1));
		assertTrue(table.add(gf2));
		assertTrue(table.add(gf3));
		assertTrue(table.remove(gf1));
		assertTrue(table.remove(gf2));
		assertTrue(table.remove(gf3));
	}
	
	@Test
	public void testContains() {
		// TODO
		FigureSet table = new OAHastable(3);
		int coords[] = {3,0,5,7};
		GeomFigure gf = new GeomFigure(coords);
		int coords2[] = {3,0,5,7};
		GeomFigure gf2 = new GeomFigure(coords2);
		assertTrue(table.add(gf));
		assertTrue(table.contains(gf));
		assertTrue(table.contains(gf2));
		int coords1[] = {3,0,5,9};
		GeomFigure gf1 = new GeomFigure(coords1);
		assertFalse(table.contains(gf1));
	}

}
