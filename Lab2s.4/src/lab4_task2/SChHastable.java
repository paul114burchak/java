package lab4_task2;

import common.FigureSet;
import common.GeomFigure;

/**
 * 
 * This class implements a set as a hash table. Hash table resolves collisions
 * Hash table is an associative array of entries. <br>
 * Each entry contains geometric figure or null. <br>
 * 
 *
 */
public class SChHastable implements FigureSet {

	private DLNode[] table;

	/**
	 * the number of non-null entries in the hashtable
	 */
	private int size;

	/**
	 * default size of the hashtable
	 */
	private final int initialCapacity = 11;

	/**
	 * The load factor is a measure of how full the hash table is allowed to get
	 * / before its capacity is automatically increased
	 */
	private double loadFactor = 0.75;

	/**
	 * It is used in multiplication hash function
	 */
	private final double A = (Math.sqrt(5) - 1) / 2;

	
	private int capacity;
	/**
	 * Constructs a new, empty hashtable with a default initial capacity (11)
	 * and load factor (0.75).
	 */
	public SChHastable() {

		// TODO
		// create an array of size equals to default initialCapacity
		this.size = 0;
		this.capacity = initialCapacity;
		this.table = new DLNode[initialCapacity];
	}

	/**
	 * Constructs a new, empty hashtable with the specified initial capacity and
	 * default load factor (0.75).
	 * 
	 * @param initialCapacity
	 *            the initial capacity of the hashtable
	 */
	public SChHastable(int initialCapacity) {

		// TODO
		// create an array of size equals to initialCapacity
		this.size = 0;
		this.capacity = initialCapacity;
		this.table = new DLNode[initialCapacity];
	}

	/**
	 * Constructs a new, empty hashtable with the specified initial capacity and
	 * the specified load factor.
	 * 
	 * @param initialCapacity
	 *            the initial capacity of the hashtable
	 * @param loadFactor
	 *            the load factor of the hashtable
	 */
	public SChHastable(int initialCapacity, double loadFactor) {

		// TODO create an array of size equals to initialCapacity
		// TODO initialize field loadFactor
		this.table = new DLNode[initialCapacity];
		this.loadFactor = loadFactor;
		this.size = 0;
		this.capacity = initialCapacity;
	}

	/**
	 * Returns the number of entries in the hashtable
	 * 
	 * @return the number of entries in the hashtable
	 */
	public int size() {

		return size;
	}

	/**
	 * Increases the capacity of and internally reorganizes this hashtable, in
	 * order to accommodate and access its entries more efficiently. This method
	 * is called when the number of elements in the hashtable exceeds this
	 * hashtable's capacity and load factor
	 */
	private void rehash() {
		// TODO
		
	}

	/**
	 * The hash function is used to calculate the hasvalue of the object gf.
	 * Choose hashing method from your variant (table 1): deletion or
	 * multiplication
	 * 
	 * @param gf
	 *            some geometric figure
	 * @return hash value - index in table
	 */
	private int hash(GeomFigure gf) {
		// TODO
		//

		return Math.abs(gf.hashCode()) % this.capacity;
	}

	@Override
	public boolean add(GeomFigure gf) {
		// TODO
		// if gf is not null and hashtable doesn't contain gf
		// ---calculate hash-value (slot number) of gf
		// ---add new Node (gf) to the linked list in the this slot
		// ---increase the size of hash table
		// ---return true
		return false;
	}

	@Override
	public boolean contains(GeomFigure gf) {
		// TODO
		// if gf is not null
		// ----Calculate hashvalue (slot number)of gf
		// ------search gf in the linked list in this slot
		return false;
	}

	@Override
	public boolean remove(GeomFigure gf) {
		// TODO
		// if gf is not null and hashtable contains gf
		// ---Calculate hashvalue of gf
		// ---if slot is not empty
		// -------delete Node (gf) from linked list
		// -------decrease the size of hash table
		// -------return true

		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO
		if(size == 0) return true;
		else return false;
	}

	@Override
	public void print() {
		// TODO
		// Output the table , where each row contains a number of the hash-table
		// slot, and the all elements that hashes to this slot.For example,

		// If a slot is empty the row contains a number of the hash-table slot
		// and the message �The slot is empty�.
		for (int i = 0; i < size; i++) {
			if (this.table[i] == null) System.out.println("The slot is empty");
			else {
				System.out.printf("%d | %d | %d|  ",i, this.table[i].data.getSquare(), this.table[i].data.getPerimeter());
				this.table[i].data.printCoordinates();
			}
		}
	}

}

class DLNode {
	GeomFigure data; // the data stored in this node.

	DLNode next; // pointer to the next node
	DLNode prev; // pointer to the previous nod

	/**
	* Construct the node of doubly-linked list with null pointers to the next
	* and previous nodes
	*
	* @param data
	* the data to stored in this node
	*/
	public DLNode(GeomFigure data) {
		this.data = data;
		next = null;
		prev = null;
	}
	
	/**
	* Construct the node of doubly-linked list and fill its fields
	*
	* @param data
	* the data to stored in this node
	* @param next
	* pointer to the next node
	* @param prev
	* pointer to the previous node
	*/
	public DLNode(GeomFigure data, DLNode next, DLNode prev) {
		this.data = data;
		this.next = next;
		this.prev = prev;
	}

}
