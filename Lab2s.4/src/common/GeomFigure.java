package common;

/**
 * Class represents geometric figure such as rectangle or triangle. Contains
 * coordinates of figure and methods to calculate area and perimeter
 */
public class GeomFigure {

	// TODO
	private int perimeter;
	private int square;
	private int coords[] = new int[4];
	private int hash = 0;

	public GeomFigure(int[] coords) {
		// TODO
		System.arraycopy(coords, 0, this.coords, 0, 4);
		this.square = Math.abs((this.coords[0] - this.coords[2])*(this.coords[1] - this.coords[3]));
		this.perimeter = Math.abs(2*((this.coords[0] - this.coords[2])+(this.coords[1] - this.coords[3])));		
		this.hash = hashCode();
	}

	/**
	 * Returns the hash code value for this geometric figure. The hash code of a
	 * figure is defined to be the sum of the hash codes of the elements in the
	 * figure, where the hash code of a null element is defined to be zero.
	 * 
	 * @return the hash code value for this object
	 */
	public int hashCode() {
		// TODO
		if (hash == 0) {
			hash = 1;
			for (int i = 0; i < coords.length; i++) {
				hash = hash * 47 + Integer.hashCode(coords[i]);
			}
		}
		return hash;
	}

	@Override
	public String toString() {
		return String.format("%-10s|  %-6d|  %-6d|", hash, perimeter, square);
	}
	
	public int getSquare() {
		return square;
	}
	
	public int getPerimeter() {
		return perimeter;
	}
	
	public void printCoordinates() {
		System.out.printf("Coordinates: (%d,%d);(%d,%d)\n",coords[0],coords[1],coords[2],coords[3]);
	}
	/**
	 * Returns the hash code value for this geometric figure. The hash code of a
	 * figure is defined to be the sum of the hash codes of the elements in the
	 * figure, where the hash code of a null element is defined to be zero.
	 * 
	 * @return the hash code value for this object
	 */
	public boolean equals(GeomFigure gf) {
		// TODO
		if (this == gf) return true;
		//if (this.hash == gf.hash) return true;
		int c = 0;
		for (int i = 0; i < coords.length; i++) {
			if (gf.coords[i] != this.coords[i]) return false;
			else c++;
		}
		if (c == 4) return true;
		return false;
//		if (gf.coords[0] != this.coords[0]) return false;
//		if (gf.coords[1] != this.coords[1]) return false;
//		if (gf.coords[2] != this.coords[2]) return false;
//		if (gf.coords[3] != this.coords[3]) return false;
//		return true;

	}

}
