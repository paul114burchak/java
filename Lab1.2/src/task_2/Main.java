package task_2;

public class Main {
	public static void main(String[] args) {

		// task 1: test1

		int valA = 15;
		int valB = 0;
		int valC = 15;

		// TODO
		// isEqial is true, if valA, valB and valC are equals, otherwise false
		// keep the result in the variable isEqial

		boolean isEqual = true;
		if (valA == valB && valA == valC) {
			isEqual = true;
			System.out.printf("%nequals%n");
		} else {
			isEqual = false;
			System.out.printf("%nnot equals%n");
		}

		// TODO
		// Output message "equals", if values are equals, otherwise "not equals"

		testTask1("1.1", valA, valB, valC, isEqual, false);

		// task 1: test2

		valA = -500;
		valB = -500;
		valC = -500;

		// TODO
		// isEqial is true, if valA, valB and valC are equals, otherwise false
		// keep the result in the variable isEqial

		isEqual = false;
		if (valA == valB && valA == valC) {
			isEqual = true;
			System.out.printf("%nequals%n");
		} else {
			isEqual = false;
			System.out.printf("%nnot equals%n");
		}

		// TODO
		// Output message "equals", if values are equals, otherwise "not equals"

		testTask1("1.2", valA, valB, valC, isEqual, true);

		// task 1: test3

		valA = -5;
		valB = 5;
		valC = 325698;

		// TODO
		// isEqial is true, if valA, valB and valC are equals, otherwise false
		// keep the result in the variable isEqial

		isEqual = true;
		if (valA == valB && valA == valC) {
			isEqual = true;
			System.out.printf("%nequals%n");
		} else {
			isEqual = false;
			System.out.printf("%nnot equals%n");
		}

		// TODO
		// Output message "equals", if values are equals, otherwise "not equals"

		testTask1("1.3", valA, valB, valC, isEqual, false);

		// task 2: test1
		// TODO
		// find the minimum value among valA, valB and valC
		// keep the result in the variable min

		int min = Integer.MAX_VALUE;
		if (valA < valB) {
			min = valA;
		} else {
			min = valB;
		}

		if (valC < min) {
			min = valC;
		}

		testTask2("2.1", valA, valB, valC, min, -5);

		// task 2: test2

		valA = -5000;
		valB = -3500;
		valC = Integer.MIN_VALUE;
		// TODO
		// find the minimum value among valA, valB and valC
		// keep the result in the variable min

		min = Integer.MAX_VALUE;

		if (valA < valB) {
			min = valA;
		} else {
			min = valB;
		}

		if (valC < min) {
			min = valC;
		}

		testTask2("2.2", valA, valB, valC, min, Integer.MIN_VALUE);

		// task 2: test3

		valA = 56;
		valB = 56;
		valC = 56;
		// TODO
		// find the minimum value among valA, valB and valC
		// keep the result in the variable min
		min = Integer.MAX_VALUE;

		if (valA < valB) {
			min = valA;
		} else {
			min = valB;
		}

		if (valC < min) {
			min = valC;
		}

		testTask2("2.3", valA, valB, valC, min, 56);

		// task 3: test1

		int weight = 86;
		int height = 170;

		// TODO
		// define Body Mass Index (BMI) category
		// hint: use solution of task4 (lab1, part1)
		// BMI <= 18.5 "Underweight"
		// BMI = 18.5 - 24.9 "Normal weight"
		// BMI = 25-29.9 "Overweight"
		// BMI >= 30.0 "Obesity"
		// keep the result in the variable category

		String category = "";

		double bmi = weight / (Math.pow(height / 100.0, 2));
		if (bmi <= 18.5) {
			category = "Underweight";
		} else {
			if (bmi > 18.5 && bmi < 25) {
				category = "Normal weight";
			} else {
				if (bmi >= 25 && bmi < 30) {
					category = "Overweight";
				} else {
					category = "Obesity";
				}
			}
		}

		testTask3("3.1", weight, height, category, "Overweight");

		// task 3: test2

		// TODO
		// define Body Mass Index (BMI) category
		// keep the result in the variable category
		weight = 50;
		category = "";

		bmi = weight / (Math.pow(height / 100.0, 2));
		if (bmi <= 18.5) {
			category = "Underweight";
		} else {
			if (bmi > 18 && bmi < 25) {
				category = "Normal weight";
			} else {
				if (bmi >= 25 && bmi < 30) {
					category = "Overweight";
				} else {
					category = "Obesity";
				}
			}
		}

		testTask3("3.2", weight, height, category, "Underweight");

		// task 3: test3

		// TODO
		// define Body Mass Index (BMI) category
		// keep the result in the variable category
		weight = 100;
		category = "";

		bmi = weight / (Math.pow(height / 100.0, 2));
		if (bmi <= 18.5) {
			category = "Underweight";
		} else {
			if (bmi > 18 && bmi < 25) {
				category = "Normal weight";
			} else {
				if (bmi >= 25 && bmi < 30) {
					category = "Overweight";
				} else {
					category = "Obesity";
				}
			}
		}

		testTask3("3.3", weight, height, category, "Obesity");

		// task 4: test1
		double x = 12.54;
		// TODO calculate y by formula (see manuals to lab1)
		double y = 0;

		if (x > 3) {
			y = Math.sin(x) / Math.sqrt(Math.pow(x, 2) + 1);
		} else {
			y = Math.log(Math.pow(x, 2) + 1) / (Math.pow(x, 2) + 1);
		}

		testTask4("4.1", x, y, -0.00209602209462795);

		// task 4: test2
		x = -12.54;
		// TODO calculate y by formula (see manuals to lab1)
		y = 0;

		if (x > 3) {
			y = Math.sin(x) / Math.sqrt(Math.pow(x, 2) + 1);
		} else {
			y = Math.log(Math.pow(x, 2) + 1) / (Math.pow(x, 2) + 1);
		}

		testTask4("4.2", x, y, 0.03200085289221526);

		// task 5: test1

		valA = 90;
		valB = 105;
		valC = 110;
		int valD = 110;
		// TODO
		// check the condition valA <= valB <= valC <= valD
		// keep the result of comparison in the variable isSorted
		boolean isSorted = false;
		if (valA <= valB && valB <= valC && valC <= valD) {
			isSorted = true;
		} else {
			isSorted = false;
		}

		testTask5("5.1", valA, valB, valC, valD, isSorted, true);

		// task 5: test2
		valD = -valD;
		// TODO
		// check the condition valA <= valB <= valC <= valD
		// keep the result of comparison in the variable isSorted
		if (valA <= valB && valB <= valC && valC <= valD) {
			isSorted = true;
		} else {
			isSorted = false;
		}

		testTask5("5.2", valA, valB, valC, valD, isSorted, false);

		// task 6: test1

		// TODO
		// calculate the arithmetic mean of only positive values valA, valB,
		// valC, valD
		// keep the result in the variable arithmMeanPositive
		double arithmMeanPositive = 0.0;
		double i = 0.0;
		if (valA > 0) {
			i++;
			arithmMeanPositive = arithmMeanPositive + valA;
		}
		if (valB > 0) {
			i++;
			arithmMeanPositive = arithmMeanPositive + valB;
		}
		if (valC > 0) {
			i++;
			arithmMeanPositive = arithmMeanPositive + valC;
		}
		if (valD > 0) {
			i++;
			arithmMeanPositive = arithmMeanPositive + valD;
		}
		
		if (i != 0) {
			arithmMeanPositive = arithmMeanPositive / i;
		}


		testTask6("6.1", valA, valB, valC, valD, arithmMeanPositive, 101.66666667);

		// task 6: test2
		// TODO
		// calculate the arithmetic mean of only positive values valA, valB,
		// valC, valD
		// keep the result in the variable arithmMeanPositive
		valA = -90;
		valB = -105;
		valC = -110;
		valD = -200;
		arithmMeanPositive = 0.0;
		i = 0.0;

		if (valA > 0) {
			i++;
			arithmMeanPositive += valA;
		}
		if (valB > 0) {
			i++;
			arithmMeanPositive += valB;
		}
		if (valC > 0) {
			i++;
			arithmMeanPositive += valC;
		}
		if (valD > 0) {
			i++;
			arithmMeanPositive += valD;
		}

		if (i != 0) {
			arithmMeanPositive = arithmMeanPositive / i;
		}

		testTask6("6.2", valA, valB, valC, valD, arithmMeanPositive, 0.0);

	}

	private static void testTask1(String message, int a, int b, int c, boolean answer, boolean expected) {
		if (answer == expected) {
			System.out.printf("%ntest %s passed%n", message);
		} else {
			System.out.printf("%ntest %s failed:  valA = %d, valB = %d, valC = %d answer: %s  expected: %s%n", message,
					a, b, c, answer ? "equals" : "not equals", expected ? "equals" : "not equals");
		}
	}

	private static void testTask2(String message, int a, int b, int c, int rez, int expected) {
		if (rez == expected) {
			System.out.printf("%ntest %s passed%n", message);
		} else {
			System.out.printf("%ntest %s failed:  valA = %d, valB = %d, valC = %d answer: %d  expected: %d%n", message,
					a, b, c, rez, expected);
		}
	}

	private static void testTask3(String message, int weight, int height, String category, String expected) {
		if (category.equals(expected)) {
			System.out.printf("%ntest %s passed%n", message);
		} else {
			System.out.printf("%ntest %s failed:  weight = %d, height = %d, answer: %s  expected: %s%n", message,
					weight, height, category, expected);
		}
	}

	private static void testTask4(String message, double x, double y, double expected) {
		if (Double.compare(y, expected) == 0) {
			System.out.printf("%ntest %s passed%n", message);
		} else {
			System.out.printf("%ntest %s failed:  x = %f, y = %f  expected: %f%n", message, x, y, expected);
		}
	}

	private static void testTask5(String message, int a, int b, int c, int d, boolean answer, boolean expected) {
		if (answer == expected) {
			System.out.printf("%ntest %s passed%n", message);
		} else {
			System.out.printf(
					"%ntest %s failed:  valA = %d, valB = %d, valC = %d, valD = %d   answer: %s  expected: %s%n",
					message, a, b, c, d, answer, expected);
		}
	}

	private static void testTask6(String message, int a, int b, int c, int d, double answer, double expected) {
		if (Math.abs(answer - expected) <= 0.1E-7) {
			System.out.printf("%ntest %s passed%n", message);
		} else {
			System.out.printf(
					"%ntest %s failed:  valA = %d, valB = %d, valC = %d, valD = %d   answer: %f  expected: %f%n",
					message, a, b, c, d, answer, expected);
		}
	}
}
